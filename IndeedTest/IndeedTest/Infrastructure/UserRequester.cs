using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Threading.Tasks;
using IndeedTest.Helpers;
using IndeedTest.Models;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace IndeedTest.Infrastructure
{
    public class UserRequester:BaseHttpRequester<User>,IUserRequester
    {
        
        public UserRequester(IHttpClientFactory httpClientFactory, ILoggerFactory loggerFactory) : base("users", httpClientFactory, loggerFactory)
        {    
        }

        public Task<IEnumerable<User>> GetAllUser()
        {
            return GetAll();
        }

        public Task<User> GetUserById(int id)
        {
            return GetById(id);
        }
    }
}