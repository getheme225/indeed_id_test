using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Threading.Tasks;
using IndeedTest.Helpers;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace IndeedTest.Infrastructure
{
    public abstract class BaseHttpRequester<T> where  T: class, new()
    {
        private readonly HttpClient _client;
        private readonly string _path;
        private readonly ILogger<BaseHttpRequester<T>> _logger;
        protected BaseHttpRequester(string path,IHttpClientFactory httpClientFactory,ILoggerFactory loggerFactory)
        {
            _path = path ??  throw new ArgumentException(nameof(path));
            _logger = loggerFactory?.CreateLogger<BaseHttpRequester<T>>()?? throw  new ArgumentException(nameof(loggerFactory));
            _client = httpClientFactory.CreateClient("JsonPlaceHolder");
        }

        protected async Task<IEnumerable<T>> GetAll()
        {
            var result = await GetRequest(_path);
            return JsonConvert.DeserializeObject<IEnumerable<T>>(result);
        }

        protected async Task<T> GetById(int id)
        {
            var endpoint = $"{_path}/{id}";
            var result = await GetRequest(endpoint);
            return JsonConvert.DeserializeObject<T>(result);
        }

        protected async Task<IEnumerable<T>> GetByQuery(NameValueCollection valueCollection)
        {
            var endpoint = _path + valueCollection.Query();
            var result = await GetRequest(endpoint);
            return JsonConvert.DeserializeObject<IEnumerable<T>>(result);
        }
        
        private async Task<string> GetRequest(string endpoint)
        {            
            try
            {
                var response = await _client.GetAsync(endpoint);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();                   
                }
                throw new HttpRequestException(response.StatusCode.ToString());
            }      
            
            catch (Exception e)
            {
               _logger.LogError(e,e.Message);
               throw;
            }
        }
    }
}