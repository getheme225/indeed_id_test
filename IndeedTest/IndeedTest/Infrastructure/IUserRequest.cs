using System.Collections.Generic;
using System.Threading.Tasks;
using IndeedTest.Models;

namespace IndeedTest.Infrastructure
{
    public interface IUserRequester
    {
        Task<IEnumerable<User>> GetAllUser();
        Task<User> GetUserById(int id);
    }
}