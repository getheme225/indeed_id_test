using System.Collections.Generic;
using System.Threading.Tasks;
using IndeedTest.Models;

namespace IndeedTest.Infrastructure
{
    public interface IAlbumRequester
    {
        Task<Album> GetAlbumById(int id);
        Task<IEnumerable<Album>> GetAllAlbums();
        Task<IEnumerable<Album>> GetUserAlbums(int userId);
    }
}