using System.Collections.Generic;
using System.Collections.Specialized;
using System.Net.Http;
using System.Threading.Tasks;
using IndeedTest.Models;
using Microsoft.Extensions.Logging;

namespace IndeedTest.Infrastructure
{
    public class AlbumRequester:BaseHttpRequester<Album>,IAlbumRequester
    {
        public AlbumRequester(IHttpClientFactory httpClientFactory,ILoggerFactory  loggerFactory) : base("albums", httpClientFactory, loggerFactory)
        {
        }

        public Task<Album> GetAlbumById(int id)
        {
            return GetById(id);
        }

        public Task<IEnumerable<Album>> GetAllAlbums()
        {
            return GetAll();
        }

        public Task<IEnumerable<Album>> GetUserAlbums(int userId)
        {
            return GetByQuery(Query(nameof(userId), userId));
        }

        private NameValueCollection Query(string key, object value)
        {
            return  new NameValueCollection{{key,$"{value}"}};
        }
    }
}