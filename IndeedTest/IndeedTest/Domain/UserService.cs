using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IndeedTest.Infrastructure;
using IndeedTest.Models;
using Microsoft.Extensions.Logging;
using IndeedTest.Helpers;
using IndeedTest.Helpers.BusinessResult;

namespace IndeedTest.Domain
{
    public class UserService:IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly IUserRequester _userRequest;

        public UserService(IUserRequester userRequester,ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory?.CreateLogger<UserService>() ?? throw new ArgumentException($"{nameof(UserService)}_{nameof(loggerFactory)}");
            _userRequest = userRequester ?? throw new ArgumentException(nameof(userRequester));
        }
        
        public async Task<BusinessResult> Users()
        {
            try
            {
                var result = await _userRequest.GetAllUser();
                foreach (var user in result)
                {
                    user.Email = user.Email.Encrypt("key");
                }

                return new BusinessSuccessResult<IEnumerable<User>>(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return new BusinessExceptionResult(e,e.Message);
            }
        }

        public async Task<BusinessResult> GetUser(int id)
        {
            try
            {
                var result = await _userRequest.GetUserById(id);
                if (result == null) return new BusinessErrorResult($"User with id: {id}  does not exist");

                result.Email = result.Email.Encrypt("key");
                return new BusinessSuccessResult<User>(result);

            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return new BusinessExceptionResult(e,e.Message);
            }
        }
    }
}