using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IndeedTest.Helpers.BusinessResult;
using IndeedTest.Infrastructure;
using IndeedTest.Models;
using Microsoft.Extensions.Logging;

namespace IndeedTest.Domain
{
    public class AlbumService:IAlbumService
    {
        private readonly ILogger<AlbumService> _logger;
        private readonly IAlbumRequester _albumRequester;
        private readonly IUserRequester _userRequest;
        
        public AlbumService(ILoggerFactory loggerFactory,IAlbumRequester albumRequester, IUserRequester userRequester)
        {
            _albumRequester = albumRequester ?? throw new ArgumentException(nameof(albumRequester));
            _userRequest = userRequester ?? throw new  ArgumentException(nameof(userRequester));
            _logger = loggerFactory?.CreateLogger<AlbumService>() ?? throw new ArgumentException($"{nameof(AlbumService)}_{nameof(loggerFactory)}.");
        }
        
        public async Task<BusinessResult> Albums()
        {
            try
            {
                var result = await _albumRequester.GetAllAlbums();
                return  new BusinessSuccessResult<IEnumerable<Album>>(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return new BusinessExceptionResult(e,e.Message);
            }
        }

        public async Task<BusinessResult> AlbumById(int id)
        {
            try
            {
                var result = await _albumRequester.GetAlbumById(id);
                
                if(result == null) 
                    return  new BusinessErrorResult($"Sorry we have not any albums with id: {id}");
                
                return new BusinessSuccessResult<Album>(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return new BusinessExceptionResult(e,e.Message);
            }
        }

        public async Task<BusinessResult> UserAlbums(int userId)
        {
            try
            {
                var user = await _userRequest.GetUserById(userId);
                if(user == null)
                    return  new BusinessErrorResult($"user with id {userId}, sorry we cant get any albums for him");
                
                var result = await _albumRequester.GetUserAlbums(userId);
                return new BusinessSuccessResult<IEnumerable<Album>>(result);
            }
            catch (Exception e)
            {
                _logger.LogError(e,e.Message);
                return new BusinessExceptionResult(e,e.Message);
            }
        }
    }
}