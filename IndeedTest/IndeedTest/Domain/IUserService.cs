using System.Threading.Tasks;
using IndeedTest.Helpers.BusinessResult;

namespace IndeedTest.Domain
{
    public interface IUserService
    {
        Task<BusinessResult> Users();
        Task<BusinessResult> GetUser(int id);
    }
}