using System.Threading.Tasks;
using IndeedTest.Helpers.BusinessResult;

namespace IndeedTest.Domain
{
    public interface IAlbumService
    {
        Task<BusinessResult> Albums();
        Task<BusinessResult> AlbumById(int id);
        Task<BusinessResult> UserAlbums(int userId);
    }
}