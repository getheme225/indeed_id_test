﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using IndeedTest.Domain;
using IndeedTest.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;

namespace IndeedTest
{
    public class Startup
    {
        
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSwaggerGen(opt =>
            {
                opt.DescribeAllEnumsAsStrings();
                opt.SwaggerDoc("v1", new Info
                {
                    Title = "Indeed Test Api",
                    Version = "v1",
                    Description = "Indeed Test Api",
                    TermsOfService = "Terms of Service"
                });
            });
            services.AddLogging();
            AddDomainServices(services);
            AddRequesterServices(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseSwagger()
                .UseSwaggerUI(c =>
                {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Indeed Test Api");
                });
            
            app.UseMvc();
        }

        private void AddDomainServices(IServiceCollection services)
        {
            services.AddSingleton<IAlbumService, AlbumService>();
            services.AddSingleton<IUserService, UserService>();
        }

        private void AddRequesterServices(IServiceCollection services)
        {
            services.AddHttpClient("JsonPlaceHolder", client =>
            {
                client.BaseAddress = new Uri(Configuration["JsonPlaceHolderBaseAddress"]);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            });
            services.AddSingleton<IAlbumRequester, AlbumRequester>();
            services.AddSingleton<IUserRequester, UserRequester>();
        }
        
        
    }
}