using System;

namespace IndeedTest.Helpers.BusinessResult
{
    public class BusinessExceptionResult : BusinessErrorResult
    {
        public Exception Exception;
        
        public BusinessExceptionResult(Exception exception,string errorMessage) : base(errorMessage)
        {
            Exception = exception;
        }
    }
}