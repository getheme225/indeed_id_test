﻿namespace IndeedTest.Helpers.BusinessResult
{
    public abstract class BusinessResult
    {
        public bool Success { get; }
        protected BusinessResult(bool success)
        {
            Success = success;
        }
    }

    public abstract class BusinessResult<T>:BusinessResult
    {
        public T Data { get; }
        
        protected BusinessResult(T data ,bool success) : base(success)
        {
            Data = data;
        }
    }
}

