namespace IndeedTest.Helpers.BusinessResult
{
    public class BusinessErrorResult : BusinessResult
    {
        public string Message { get; }

        public BusinessErrorResult(string errorMessage):base(false)
        {
            Message = errorMessage;
        }
    }
}