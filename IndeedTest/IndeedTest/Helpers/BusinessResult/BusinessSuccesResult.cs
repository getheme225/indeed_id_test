namespace IndeedTest.Helpers.BusinessResult
{
    public class BusinessSuccessResult<T>:BusinessResult<T> 
    {

        public BusinessSuccessResult(T data) : base(data,true)
        {
        }
    }

    public class BusinessSuccessResult : BusinessResult
    {
        public BusinessSuccessResult() : base(true)
        {
        }
    }
}