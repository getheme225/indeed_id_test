using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Microsoft.EntityFrameworkCore.Internal;

namespace IndeedTest.Helpers
{
    public static class Utilits
    {
        public static string Query(this NameValueCollection valueCollection)
        {
            if (valueCollection?.Any() != true) return null;
            var array = (from key in valueCollection.AllKeys
                    from value in valueCollection.GetValues(key)
                    select $"{HttpUtility.UrlEncode(key)}={HttpUtility.UrlEncode(value)}")
                .ToArray();
            return "?" + string.Join("&", array);
        }

        public static string Encrypt(this string password,string key)
        {
            var buffer = Encoding.UTF8.GetBytes(password);
            var keyBytes = KeyGenerator(key);

            using (var inputStream = new MemoryStream(buffer, false))
            using (var outputStream = new MemoryStream())
            using (var aes = new AesManaged { Key = keyBytes })
            {
                var iv = aes.IV;
                outputStream.Write(iv, 0, iv.Length);
                outputStream.Flush();

                var encryptor = aes.CreateEncryptor(keyBytes, iv);
                using (var cryptoStream = new CryptoStream(outputStream, encryptor, CryptoStreamMode.Write))
                {
                    inputStream.CopyTo(cryptoStream);
                }

                return Convert.ToBase64String(outputStream.ToArray());
            }  
        }
             
        static byte[] KeyGenerator(string key)
        {
            byte[] keyBytes;
            using (var sha = new SHA256Managed())
            {
                keyBytes = sha.ComputeHash(Encoding.UTF8.GetBytes(key));
            }
            return keyBytes;
        }

    }
}