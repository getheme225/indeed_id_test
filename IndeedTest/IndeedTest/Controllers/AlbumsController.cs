using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IndeedTest.Domain;
using IndeedTest.Helpers.BusinessResult;
using IndeedTest.Models;
using Microsoft.AspNetCore.Mvc;

namespace IndeedTest.Controllers
{
    [Route("api/[Controller]")]
    public class AlbumsController:Controller
    {
        private readonly IAlbumService _albumService;

        public AlbumsController(IAlbumService albumService)
        {
            _albumService = albumService ?? throw  new ArgumentException(nameof(albumService));         
        }

        [HttpGet]
        [ProducesResponseType(typeof(BusinessSuccessResult<IEnumerable<Album>>),200)]
        [ProducesErrorResponseType(typeof(BusinessErrorResult))]
        [ProducesResponseType(typeof(BusinessExceptionResult),500)]
        public async Task<ActionResult> Get()
        {

            var result = await _albumService.Albums();
            if (result.Success) return Ok((BusinessSuccessResult<IEnumerable<Album>>) result);

            if (!result.Success && !(result is BusinessExceptionResult))
            {
                return BadRequest((BusinessErrorResult) result);
            }
              
            return InternalServerError(result);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BusinessSuccessResult<Album>),200)]
        [ProducesErrorResponseType(typeof(BusinessErrorResult))]
        [ProducesResponseType(typeof(BusinessExceptionResult),500)]
        public async Task<ActionResult> GetById(int id)
        {
            var result = await _albumService.AlbumById(id);
            if (result.Success) return Ok((BusinessSuccessResult<Album>) result);

            if (!result.Success && !(result is BusinessExceptionResult))
            {
                return BadRequest((BusinessErrorResult) result);
            }
              
            return InternalServerError(result);
        }
        
        [HttpGet("User/{id}")]
        [ProducesResponseType(typeof(BusinessSuccessResult<IEnumerable<Album>>),200)]
        [ProducesErrorResponseType(typeof(BusinessErrorResult))]
        [ProducesResponseType(typeof(BusinessExceptionResult),500)]
        public async Task<ActionResult> GetAlbumByUser(int userId)
        {
            var result = await _albumService.AlbumById(userId);
            if (result.Success) return Ok((BusinessSuccessResult<IEnumerable<Album>>) result);

            if (!result.Success && !(result is BusinessExceptionResult))
            {
                return BadRequest((BusinessErrorResult) result);
            }
              
            return InternalServerError(result);
        }
        
        private static ObjectResult InternalServerError(BusinessResult result)
        {
            return new ObjectResult((BusinessExceptionResult) result) {StatusCode = 500};
        }
    }
}