﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using IndeedTest.Domain;
using IndeedTest.Helpers.BusinessResult;
using IndeedTest.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace IndeedTest.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService, ILoggerFactory loggerFactory)
        {
            _userService = userService ?? throw  new ArgumentException(nameof(userService));         
        }

        [HttpGet]
        [ProducesResponseType(typeof(BusinessSuccessResult<IEnumerable<User>>),200)]
        [ProducesErrorResponseType(typeof(BusinessErrorResult))]
        [ProducesResponseType(typeof(BusinessExceptionResult),500)]
        public async Task<ActionResult> Get()
        {

                var result = await _userService.Users();
                if (result.Success) return Ok((BusinessSuccessResult<IEnumerable<User>>) result);

                if (!result.Success && !(result is BusinessExceptionResult))
                {
                    return BadRequest((BusinessErrorResult) result);
                }
                return InternalServerError(result);
        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(BusinessSuccessResult),200)]
        [ProducesErrorResponseType(typeof(BusinessErrorResult))]
        [ProducesResponseType(typeof(BusinessExceptionResult),500)]
        public async Task<ActionResult> GetById(int id)
        {
            var result = await _userService.GetUser(id);
            if (result.Success) return Ok((BusinessSuccessResult<User>) result);

            if (!result.Success && !(result is BusinessExceptionResult))
            {
                return BadRequest((BusinessErrorResult) result);
            }
              
            return InternalServerError(result);
        }

        private ObjectResult InternalServerError(BusinessResult result)
        {
            return new ObjectResult((BusinessExceptionResult) result) {StatusCode = 500};
        }
    }
}